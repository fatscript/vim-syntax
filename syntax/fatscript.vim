" Vim syntax file
" Language: FatScript v3.0.0
" URL: https://gitlab.com/fatscript/vim-syntax
" Original author: Antonio Prates
" License: GPL version 3 or newer

if exists("b:current_syntax")
  finish
endif

" Clear old syntax
syn clear

" Define patterns
syn match fatScriptParentheses "[\(\)\[\]{}]"
syn match fatScriptType "\<[A-Z][A-Za-z0-9_]*\>"
syn match fatScriptEntry "\<[a-z][A-Za-z0-9_]*\>"
syn match fatScriptEmbedded "\$[A-Za-z]*"
syn match fatScriptNumber "\<\d*\>\|\<\d*\.\d*\>\|\<0x[0-9a-fA-F]+\>"
syn keyword fatScriptKeyword false null root self true infinity
syn match fatScriptSymbolic "\V\<-\|->\|<>\|=>\|~\|@\|?\|:\|.\|.<\|>>\|<<"
syn match fatScriptText /'\([^'\\]\|\\.\)*'\|"\([^"\\]\|\\.\)*"/
syn match fatScriptComment "#.*"
syn match fatScriptReminder "\s\(FIXME\|TODO\|XXX\)\(\s\|:\s\)"
syn match fatScriptDebug "\$break"

" Match quoted strings
syn region fatScriptDoubleQuotedString start=+"+ skip=+\\"+ end=+"+ contains=fatScriptEscape
syn region fatScriptSingleQuotedString start=+'+ skip=+\\'+ end=+'+ contains=fatScriptEscape

" Define the escape sequences within strings
syn match fatScriptEscape contained /\\\(['\"{ebnrt]\|[0-7]\{1,3}\)/

" Define highlights
hi def link fatScriptParentheses Delimiter
hi def link fatScriptType Type
hi def link fatScriptEntry Identifier
hi def link fatScriptEmbedded Special
hi def link fatScriptNumber Number
hi def link fatScriptKeyword Keyword
hi def link fatScriptSymbolic Operator
hi def link fatScriptText String
hi def link fatScriptEscape SpecialChar
hi def link fatScriptComment Comment
hi def link fatScriptReminder Todo
hi def link fatScriptDebug Error

let b:current_syntax = "fatscript"
