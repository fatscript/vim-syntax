# FatScript Vim Syntax Highlighting

This repository provides syntax highlighting for [FatScript](https://fatscript.org/en/) files in Vim and Neovim.

## Installation

### Using packer.nvim

If you're using [packer.nvim](https://github.com/wbthomason/packer.nvim) as your package manager, you can add FatScript syntax highlighting to your Neovim setup including the following line in your `packer` startup like so:

```lua
use { 'https://gitlab.com/fatscript/vim-syntax', as = 'fatscript' }
```

### Using lazy.nvim

For users of [lazy.nvim](https://github.com/folke/lazy.nvim), you can include the plugin in your setup like so:

```lua
{ 'https://gitlab.com/fatscript/vim-syntax', name = 'fatscript' }
```

### Manual Installation

For those who prefer manual installation or are using Vim without a package manager, follow these steps:

1. Download the `fatscript.vim` file from [here](https://gitlab.com/fatscript/vim-syntax/raw/main/syntax/fatscript.vim?inline=false).
2. Copy the `fatscript.vim` file to your Vim or Neovim syntax directory. This is usually `~/.vim/syntax/` for Vim or `~/.config/nvim/syntax/` for Neovim.

   ```bash
   cp fatscript.vim ~/.vim/syntax/  # for Vim
   # or
   cp fatscript.vim ~/.config/nvim/syntax/  # for Neovim
   ```

3. Add the following line to your `vimrc` or `init.vim` to ensure it recognizes files with the `.fat` extension as FatScript files:

   ```vim
   au BufRead,BufNewFile *.fat set filetype=fatscript
   ```

## Usage

Once installed, Vim or Neovim will automatically apply FatScript syntax highlighting to files with the `.fat` extension.

## Support

For issues, suggestions, or contributions, please use the [issues section](https://gitlab.com/fatscript/vim-syntax/issues) of this repository.

### Donations

Did you find `vim-syntax` useful and would like to say thanks?

[Buy me a coffee](https://www.buymeacoffee.com/aprates)

## License

[GPLv3](LICENSE) © 2024 Antonio Prates.
